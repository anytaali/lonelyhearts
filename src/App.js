import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Fabric } from 'office-ui-fabric-react/lib/Fabric';
import Main from "./screens/Main.js"
class App extends Component {
  constructor(props){
    super(props);
  }
  render() {
    return (
      <Fabric>
        <div className="App">
          <div className="header">
            <h1>Lonely Hearts Dating Service</h1>
          </div>
          <Main/>
        </div>
      </Fabric>

    );
  }
}

export default App;
