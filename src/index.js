import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { loadTheme } from 'office-ui-fabric-react/lib/Styling';
import { initializeIcons } from '@uifabric/icons';

// Register icons and pull the fonts from the default SharePoint cdn:
initializeIcons();
loadTheme({
  palette: {
    'themePrimary': 'purple'
  }
});
ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
